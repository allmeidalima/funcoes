class Args():
    
    def nanana(self):
        print("5 MINUTOS POR DIA")

    #nanana()

    def ela_fala_ingles_ela(self, arg):
        print(arg)

    #ela_fala_ingles_ela("Onde? No Duolingo...")

    def ele_nao_fala_ingles_ainda(self, *args):
        print(args)
        print(type(args))

    #ele_nao_fala_ingles_ainda("Porque ele não usa Duo", "A Lili enche meu saco todo dia", "Quase que ele aprende klingon")

    def ele_nao_fala_ingles_ainda(self, *args):
        for n in args:
            print(n)

    #ele_nao_fala_ingles_ainda("Porque ele não usa Duo", "A Lili enche meu saco todo dia", "Quase que ele aprende klingon")

    def soma(self, *args):
        print(sum(args))

    #ums = [1,2,3]
    #soma(*nums)
