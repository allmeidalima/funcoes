from args import Args
from kwargs import Kwargs

opcao = input("1.args, 2.kwargs: ")
if opcao == "1":
    Args().soma(*[1,2,3])
elif opcao == "2":
    dados = Kwargs().dict
    Kwargs().series(**dados)