class Kwargs():
    def ela_partiu(self, **kwargs):
        print(type(kwargs))
        print(kwargs)

    #ela_partiu(dev_sec_ops="E a boticario levou", na_boticario="Não levou não, uhuum")

    def ela_partiu(self, **kwargs):
        for k, v in kwargs.items():
            print(k, v)

    #ela_partiu(dev_sec_ops="E a boticario levou", na_boticario="Não levou não, uhuum")

    def series(self, **kwargs):
        for nome, ano in kwargs.items():
            print(f"{nome} foi lançada em {ano}")

    dict = {
        "Onisciente" : 2020,
        "Better Than Us" : 2018,
        "Mr. Robot" : 2015,
        "Sexify" : 2021,
        "Black Mirror" : 2011,
        "Love, Death and Robots" : 2019
    }

    #series(**dict)